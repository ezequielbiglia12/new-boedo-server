package com.server.newboedoserver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnSignIn;
    TextView txtSlogan;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSignIn = findViewById(R.id.btnSignIn);
        txtSlogan = findViewById(R.id.txtSlogan);
        //tipo de letra
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/abrilfatfaceregular.ttf");
        txtSlogan.setTypeface(face);

       btnSignIn.setOnClickListener(v -> {
           Intent signIn=new Intent(MainActivity.this,SignIn.class);
           startActivity(signIn);
       });

    }
}
